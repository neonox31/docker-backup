#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting verify command..."
eval duplicity verify ${DUPLICITY_VERIFY_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET} ${SOURCE}
info "done."