# Duplicity backup 🐳 container

Inspired from https://github.com/futurice/docker-volume-backup

This [alpine](https://hub.docker.com/_/alpine/) based image wraps [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) and a cron scheduler.
This image is available for both x86 and arm32v6 platform so it could be easily used for raspebrry pi boards.

## Container environment variables

| Variable                                    | Description                                                                                                         | Default value                           |
|:--------------------------------------------|:--------------------------------------------------------------------------------------------------------------------|:----------------------------------------|
| BACKUP_CRON_EXPRESSION                      | [Cron](https://crontab.guru/) expression for periodic backups                                                       | @daily                                  |
| DUPLICITY_BACKUP_ARGS                       | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) backup args for default sub command                 | *Empty*                                 |
| DUPLICITY_BACKUP_FULL_ARGS                  | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) backup args for `full` sub command                  | *Empty*                                 |
| DUPLICITY_BACKUP_INCREMENTAL_ARGS                   | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) backup args for `incremental` sub command                   | *Empty*                                 |
| DUPLICITY_CLEANUP_ARGS                      | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `cleanup` sub command                      | *Empty*                                 |
| DUPLICITY_COLLECTION_STATUS_ARGS            | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `collection-status` sub command            | *Empty*                                 |
| DUPLICITY_COMMON_ARGS                       | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) common args for all sub commands                    | --no-encryption --allow-source-mismatch |
| DUPLICITY_LIST_CURRENT_FILES_ARGS           | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `list-current-files` sub command           | *Empty*                                 |
| DUPLICITY_REMOVE_ALL_BUT_N_FULL_ARGS        | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `remove-all-but-n-full` sub command        | *Empty*                                 |
| DUPLICITY_REMOVE_ALL_INC_OF_BUT_N_FULL_ARGS | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `remove-all-inc-of-but-n-full` sub command | *Empty*                                 |
| DUPLICITY_REMOVE_OLDER_THAN_ARGS            | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `remove-older-than` sub command            | *Empty*                                 |
| DUPLICITY_RESTORE_ARGS                      | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `restore` sub command                      | --force                                 |
| DUPLICITY_VERIFY_ARGS                       | [Duplicity](http://duplicity.nongnu.org/vers7/duplicity.1.html) args for `verify` sub command                       | *Empty*                                 |
| SOURCE                                      | Source folder of files to backup                                                                                    | /source                                 |
| TARGET                                      | Backup destination                                                                                                  | /target                                 |

## Commands

ℹ For each command you can use `docker run` command if no container exists but you can also use with `docker exec` command if you already have a running container.

### Backup

#### Run periodic backup (default behavior)

`docker run -d -e BACKUP_CRON_EXPRESSION="0 4 * * *" -e DUPLICITY_COMMON_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity`

#### Full backup

`docker run -it --rm -e DUPLICITY_BACKUP_FULL_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity full`

#### Full backup using rsync over SSH

`docker run -it --rm -e DUPLICITY_BACKUP_ARGS="--verbosity debug" -e DUPLICITY_COMMON_ARGS="--no-encryption --allow-source-mismatch --rsync-options='-v -e \\\"ssh -i /sshkey\\\"'" -e TARGET=rsync://user@host://tmp -v /tmp/source:/source -v ~/.ssh/id_rsa:/sshkey:ro -v ~/.ssh/known_hosts:/etc/ssh/ssh_known_hosts:ro neonox31/duplicity full`

#### Incremental backup

`docker run -it --rm -e DUPLICITY_BACKUP_INCREMENTAL_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity incremental`


### Restore backup

#### Restore all latest backed-up files

`docker run -it --rm -e DUPLICITY_RESTORE_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source -v /tmp/target:/target:ro neonox31/duplicity restore`

#### Restore only a specific file of latest backup

`docker run -it --rm -e SOURCE="/source/foo" -e DUPLICITY_RESTORE_ARGS="--verbosity debug --file-to-restore foo" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source -v /tmp/target:/target:ro neonox31/duplicity restore`

#### Restore only a specific file which was backed-up 3 days ago

`docker run -it --rm -e SOURCE="/source/foo" -e DUPLICITY_RESTORE_ARGS="--verbosity debug --file-to-restore foo --time 3D" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source -v /tmp/target:/target:ro neonox31/duplicity restore`

ℹ You could refer to duplicity time formats documentation [here](http://duplicity.nongnu.org/vers7/duplicity.1.html#sect8)

### List backed up files

`docker run -it --rm -e DUPLICITY_LIST_CURRENT_FILES_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/target:/target:ro neonox31/duplicity list-current-files`

### List backup collections

`docker run -it --rm -e DUPLICITY_COLLECTION_STATUS_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/target:/target:ro neonox31/duplicity collection-status`

### Verify backup

`docker run -it --rm -e DUPLICITY_VERIFY_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target:ro neonox31/duplicity verify`

### Remove all backups (full or incremental) but keeps only the last 3 full

`docker run -it --rm -e DUPLICITY_REMOVE_ALL_BUT_N_FULL_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity remove-all-but-n-full 3`

### Remove all incremental backups but keeps only the last 3 full

`docker run -it --rm -e DUPLICITY_REMOVE_ALL_INC_OF_BUT_N_FULL_ARGS="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity remove-all-inc-of-but-n-full 3`

### Remove all backups (full or incremental) older than 3 months

`docker run -it --rm -e DUPLICITY_REMOVE_OLDER_THAN_ARGS ="--verbosity debug" -v /tmp/.cache/duplicity:/root/.cache/duplicity -v /tmp/source:/source:ro -v /tmp/target:/target neonox31/duplicity remove-older-than 3M`

ℹ You could refer to duplicity time formats documentation [here](http://duplicity.nongnu.org/vers7/duplicity.1.html#sect8)

## Docker control

This image includes `docker` binary and can take control over your running side containers to stop them before backup/restore and restart them after.
You could also execute backup pre-script and restore post-script inside your side containers.

⚠ Don't forget to specify docker socket as volume for duplicity container using `-v /var/run/docker.sock:/var/run/docker.sock:ro`

### Stop side containers before backup/restore

This is useful to make a safer backup or restore.
To make it working you should specify some labels into your side containers, for example :

`docker run --rm --label backup.stop-before=true influxdb:1.5.4`

or

`docker run --rm --label restore.stop-before=true influxdb:1.5.4`

### Execute a script inside your side containers before backup

Mark your side container with a specific label :

`docker run --rm --label backup.stop-before=true --label backup.pre-script="influxd backup -portable /tmp/influxdb" influxdb:1.5.4`

### Execute a script inside your side containers after restore

Mark your side container with a specific label :

`docker run --rm --label restore.stop-before=true --label restore.post-script="influxd restore -portable /tmp/influxdb" influxdb:1.5.4`
