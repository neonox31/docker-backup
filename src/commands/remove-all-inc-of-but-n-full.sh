#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting remove-all-inc-of-but-n-full command..."
eval duplicity remove-all-inc-of-but-n-full "${@}" ${DUPLICITY_REMOVE_ALL_INC_OF_BUT_N_FULL_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET}
info "done."